from django.db import models

# Create your models here.

class State(models.Model):
   id=models.AutoField(primary_key=True)
   StateNAme=models.CharField(max_length=100)

   class Meta:
      db_table='tblState'

class City(models.Model):
   id = models.AutoField(primary_key=True)
   CityName=models.CharField(max_length=100)
   State=models.ForeignKey('State',on_delete=models.CASCADE)

   class Meta:
      db_table='tblCity'

class TravelType(models.Model):
   id=models.AutoField(primary_key=True)
   Name=models.CharField(max_length=100)

   class Meta:
      db_table='tblTravelType'

class Leader(models.Model):
   id = models.AutoField(primary_key=True)
   CodeMelli = models.CharField(max_length=50)
   Name = models.CharField(max_length=50)
   LastName = models.CharField(max_length=50)
   FatherName = models.CharField(max_length=50)
   CertificateNumber= models.CharField(max_length=50)
   BirthDate = models.DateField()
   State = models.ForeignKey('State',on_delete=models.PROTECT)
   CityID = models.ForeignKey('City',on_delete=models.PROTECT)
   TravelType=models.ForeignKey('TravelType',on_delete=models.PROTECT)
   Address = models.TextField()
   Tel = models.CharField(max_length=50)
   TelCode = models.CharField(max_length=50)
   PostalCode = models.CharField(max_length=50)
   Mobile = models.CharField(max_length=50)
   Email = models.EmailField()



   class Meta:
      db_table = "tblLeader"

class Member(models.Model):
   id = models.AutoField(primary_key=True)
   Leader = models.ForeignKey('Leader', on_delete=models.CASCADE)
   CodeMelli=models.CharField(max_length=50)
   Name=models.CharField(max_length=50)
   LastName=models.CharField(max_length=50)
   CertificateNumber=models.CharField(max_length=50)
   BirthDate=models.DateField()

   class Meta:
    db_table="tblMember"



